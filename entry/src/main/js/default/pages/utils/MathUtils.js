export default class MathUtils {
    static dist(pos1, pos2) {
        return Math.sqrt((pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y) * (pos1.y - pos2.y))
    }

    static toRadians(angle) {
        return angle * Math.PI / 180
    }

    static toDegrees(radian) {
        return radian * 180 / Math.PI
    }

    //根据起点，距离，角度计算另一个点
    static calculatePoint(startPoint, length, angle) {
        //注意这里的角度是逆时针为正，顺时针为负；跟鸿蒙的刚好相反，因此要对angle取反
        let deltaX = (Math.cos(MathUtils.toRadians(angle)) * length);
        let deltaY = (-Math.sin(MathUtils.toRadians(angle)) * length);
        return {
            x: startPoint.x + deltaX, y: startPoint.y + deltaY
        }
    }

    //a,b变成整数的乘法因子，比如1.2，1.6，其结果应该是10，即求a,b分母的最小公倍数
    static getMulFactorToInt(...num) {
        let denominator = null
        for (let i = 0;i < num.length; i++) {
            if (denominator == null) denominator = MathUtils.getFraction(num[i])[1]
            else denominator = MathUtils.getLcm(denominator, MathUtils.getFraction(num[i])[1])
        }
    }

    //求整数a,b的最小公约数
    static getGcd(a, b) { //a,b是整数
        if (a <= 0 || b <= 0) throw Error(`${a},${b} must greater than zero`)
        let max = Math.max(a, b);
        let min = Math.min(a, b);
        if (max % min === 0) {
            return min;
        } else {
            return MathUtils.getGcd(max % min, min);
        }
    }
    //得到自然数a的最简分数形式，比如0.2-->[1,5]
    static getFraction(x) {
        if (x <= 0) throw Error(`${x} must greater than 0`)
        let t = x
        let y = String(t).indexOf(".") + 1; //获取小数点的位置，若x是整数，indexOf返回的是-1
        let count = y == 0 ? 0 : String(t).length - y; //获取小数点后的个数
        if (count >= 16) {
            t = 1 / x
            y = String(t).indexOf(".") + 1; //获取小数点的位置
            count = y == 0 ? 0 : String(t).length - y; //获取小数点后的个数
        }
        let m = Math.pow(10, count)
        let mt = m * t
        let gcd = MathUtils.getGcd(mt, m)
        //console.log(`m:${m},n:${mt},gcd:${gcd},count:${count}`)
        return [(t == x ? mt : m) / gcd, (t == x ? m : mt) / gcd] //因为取倒数，所以分子分母顺序对调
    }
    //求整数a,b的最小公倍数
    static getLcm(a, b) {
        if (a <= 0 || b <= 0) throw Error(`${a},${b} must greater than zero`)
        return a * b / MathUtils.getGcd(a, b);
    }
    //求多个数的醉倒公约数
    static getGcds(...num) {
        if (num.length == 1) return num[0]
        else if (num.length == 2) return MathUtils.getGcd(num[0], num[1])
        let gcd = MathUtils.getGcd(num[0], num[1])
        for (let i = 2;i < num.length; i++) {
            gcd = MathUtils.getGcd(gcd, num[2])
        }
        return gcd
    }
    //求多个数的最小公倍数
    static getLcms(...num) {
        if (num.length == 1) return num[0]
        else if (num.length == 2) return MathUtils.getLcm(num[0], num[1])
        let lcm = MathUtils.getLcm(num[0], num[1])
        for (let i = 2;i < num.length; i++) {
            lcm = MathUtils.getLcm(lcm, num[2])
        }
        return lcm
    }

    //1阶贝塞尔曲线
    static onebsr(t, p1, p2) {
        return p1 + (p2 - p1) * t;
    }

    //2阶贝塞尔曲线
    static twobsr(t, p1, cp1, p2) {
        return ((1 - t) * (1 - t)) * p1 + 2 * t * (1 - t) * cp1 + t * t * p2;
    }
    //3阶贝塞尔曲线
    static threebsr(t, p1, cp1, cp2, p2) {
        return p1 * (1 - t) * (1 - t) * (1 - t) + 3 * cp1 * t * (1 - t) * (1 - t) + 3 * cp2 * t * t * (1 - t) + p2 * t * t * t;
    }

    /**
     * @desc 一阶贝塞尔
     * @param {number} t 当前百分比
     * @param {Array} p1 起点坐标
     * @param {Array} p2 终点坐标
     */
    static oneBezier(t, p1, p2) {
        let x = p1.x + (p2.x - p1.x) * t;
        let y = p1.y + (p2.y - p1.y) * t;
        return {
            x: x, y: y
        };
    }

    /**
     * @desc 二阶贝塞尔
     * @param {number} t 当前百分比
     * @param {Array} p1 起点坐标
     * @param {Array} p2 终点坐标
     * @param {Array} cp 控制点
     */
    static twoBezier(t, p1, cp, p2) {
        let u = 1 - t;
        let uu = u * u;
        let tt = t * t;
        let tu = u * t;
        let x = uu * p1.x + 2 * tu * cp.y + tt * p2.x;
        let y = uu * p1.y + 2 * tu * cp.y + tt * p2.y;
        return {
            x: x, y: y
        };
    }

    /**
     * @desc 三阶贝塞尔
     * @param {number} t 当前百分比
     * @param {Array} p1 起点坐标
     * @param {Array} p2 终点坐标
     * @param {Array} cp1 控制点1
     * @param {Array} cp2 控制点2
     */
    static threeBezier(t, p1, cp1, cp2, p2) {
        let u = 1 - t;
        let uuu = u * u * u;
        let tuu = t * u * u;
        let ttu = t * t * u;
        let ttt = t * t * t;
        let x =
            p1.x * uuu +
            3 * cp1.x * tuu +
            3 * cp2.x * ttu +
            p2.x * ttt;

        let y =
            p1.y * uuu +
            3 * cp1.y * tuu +
            3 * cp2.y * ttu +
            p2.y * ttt;
        return {
            x: x, y: y
        }
    }

    //获取三阶贝塞尔曲线的切线,对三阶贝塞尔求导
    static threeBezierTangent(t, p1, cp1, cp2, p2) {
        let u = 1 - t;
        let uu = u * u;
        let tu = t * u;
        let tt = t * t;

        let x = -3 * p1.x * uu + 3 * cp1.x * (uu - 2 * tu) + 3 * cp2.x * (2 * tu - tt) + 3 * p2.x * tt
        let y = -3 * p1.y * uu + 3 * cp1.y * (uu - 2 * tu) + 3 * cp2.y * (2 * tu - tt) + 3 * p2.y * tt
        return {
            x: x, y: y
        };
    }

    //求<AOB，单位为角度
    static includedAngle(O, A, B) {
        // OA的长度
        let OALength = Math.sqrt((A.x - O.x) * (A.x - O.x) + (A.y - O.y) * (A.y - O.y));
        // OB的长度
        let OBLength = Math.sqrt((B.x - O.x) * (B.x - O.x) + (B.y - O.y) * (B.y - O.y));
        // OA*OB=(Ax-Ox)*(Bx-Ox)+(Ay-Oy)*(By-Oy)
        let AOB = (A.x - O.x) * (B.x - O.x) + (A.y - O.y) * (B.y - O.y);
        // cosAOB = (OA*OB)/(|OA|*|OB|)
        let cosAOB = AOB / (OALength * OBLength);

        // 角度 -- 反余弦
        let angleAOB = MathUtils.toDegrees(Math.acos(cosAOB));

        let OAxOB = (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
        let sinAOB = OAxOB / (OALength * OBLength);
        //旋转坐标系的角度逆时针为正，而系统的点击坐标系却是顺时针为正，因此角度是相反
        return -(sinAOB > 0 ? 1 : -1) * angleAOB;
    }
}