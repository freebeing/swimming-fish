import MathUtils from '../utils/MathUtils.js';
import animator from '@ohos.animator';

export default {
    data: {
        title: 'World',
        HEAD_RADIUS: 25,
        middlePoint: null,
        BODY_LENGTH: null,
        FIND_FINS_LENGTH: null,
        FINS_LENGTH: null,
        BIG_CIRCLE_RADIUS: null,
        MIDDLE_CIRCLE_RADIUS: null,
        SMALL_CIRCLE_RADIUS: null,
        FIND_MIDDLE_CIRCLE_LENGTH: null,
        FIND_SMALL_CIRCLE_LENGTH: null,
        FIND_TRIANGLE_LENGTH: null,
        OTHER_ALPHA: 0,
        headPoint: null,
        //绘制相关
        canvas: null,
        fishMainAngle: 90,
        currentValue: 0,
        defaultFillStyle: 'rgba(244, 92,71,0.5)',
        x: 0,
        y: 0, //左上角的坐标
        swimBuff: 1,
        //动画
        fishAnim: null,
    },
    onInit() {
        this.setHeadRadius(this.HEAD_RADIUS)
    },
    move(x, y) {
        this.x = x
        this.y = y
    },
    getPosition() {
        return {
            x: this.x,
            y: this.y
        }
    },
    moveByCenter(x, y) {
        this.x = x - this.middlePoint.x
        this.y = y - this.middlePoint.y
    },
    getCenterPosition() { //返回的是小鱼中心坐标
        return {
            x: this.x + this.middlePoint.x,
            y: this.y + this.middlePoint.y
        }
    },
    getHeadPosition() {
        return {
            x: this.x + this.headPoint.x,
            y: this.y + this.headPoint.y
        }
    },
    getHeadRadius() {
        return this.HEAD_RADIUS
    },
    onAttached() {
        setTimeout(() => { //这里需要延迟得到canvas
            this.canvas = this.$refs.fishcanvas.getContext('2d', {
                antialias: true
            });
            this.onDraw()
            this.startAnimation()
        }, 200)
    },
    setFishMainAngle(angle) {
        this.fishMainAngle = angle;
    },
    startAnimation() {
        //console.log("startAnimation begin====>");
        if (this.fishAnim == null) {
            var options = {
                duration: 5 * 1000,
                easing: 'linear',
                iterations: -1,
                direction: "normal",
                fill: "none",
                begin: 0,
                end: 1200 //必须是1200的倍数，1.5/360与1.2/360的最小公倍数是1200
            };
            this.fishAnim = animator.createAnimator(options)
            this.fishAnim.onframe = (value) => { //function会改this
                //console.log("startAnimation onframe====>" + value);
                this.currentValue = Number(value)
                this.onDraw()
            };
        }
        this.fishAnim.play();
        console.log("startAnimation ====>");
    },
    onSwimStart(){
        this.swimBuff = 3
    },
    onSwimFinish(){
        this.swimBuff = 1
    },
    getMiddlePoint() {
        return this.middlePoint
    },
    getHeadPoint(angel) {
        return MathUtils.calculatePoint(this.middlePoint,
            this.BODY_LENGTH / 2 + this.HEAD_RADIUS, angel)
    },
    onDraw() {
        this.clearCanvas()
        let fishAngle = (this.fishMainAngle + Math.sin(MathUtils.toRadians(this.currentValue * 1)) * 4);
        this.headPoint = MathUtils.calculatePoint(this.middlePoint, this.BODY_LENGTH / 2, fishAngle)
        //console.log('fishHead:-->'+JSON.stringify(this.headPoint))
        this.drawHead(this.canvas, fishAngle)
        //绘制左右鱼鳍
        let leftFinsPoint = MathUtils.calculatePoint(this.headPoint, this.FIND_FINS_LENGTH, fishAngle + 110);
        this.drawFins(this.canvas, leftFinsPoint, this.FIND_FINS_LENGTH, fishAngle, false);
        let rightFinsPoint = MathUtils.calculatePoint(this.headPoint, this.FIND_FINS_LENGTH, fishAngle - 110);
        this.drawFins(this.canvas, rightFinsPoint, this.FIND_FINS_LENGTH, fishAngle, true);
        // 身体的底部的中心点
        let bodyBottomCenterPoint = MathUtils.calculatePoint(this.headPoint, this.BODY_LENGTH, fishAngle - 180);
        // 绘制节肢1
        let middleCircleCenterPoint = this.drawSegment(this.canvas, bodyBottomCenterPoint,
            this.BIG_CIRCLE_RADIUS,
            this.MIDDLE_CIRCLE_RADIUS,
            this.FIND_MIDDLE_CIRCLE_LENGTH, fishAngle, true);
        this.drawSegment(this.canvas, middleCircleCenterPoint, this.MIDDLE_CIRCLE_RADIUS, this.SMALL_CIRCLE_RADIUS,
            this.FIND_SMALL_CIRCLE_LENGTH, fishAngle, false);

        let findEdgeLength = Math.abs(Math.sin(MathUtils.toRadians(this.currentValue * 1.2)) * this.BIG_CIRCLE_RADIUS);
        // 绘制大三角形
        this.drawTriangle(this.canvas, middleCircleCenterPoint, this.FIND_TRIANGLE_LENGTH, findEdgeLength, fishAngle);
        let findEdgeLengthS = Math.abs(Math.sin(MathUtils.toRadians(this.currentValue * 1.2 - 90)) * this.BIG_CIRCLE_RADIUS);
        // 绘制小三角形
        this.drawTriangle(this.canvas, middleCircleCenterPoint, this.FIND_TRIANGLE_LENGTH * 0.9, findEdgeLengthS * 0.6, fishAngle);
        this.drawBody(this.canvas, this.headPoint, bodyBottomCenterPoint, fishAngle)
    },
    /**
     * 画鱼身
     *
     * @param headPoint
     * @param bodyBottomCenterPoint
     */
    drawBody(canvas, headPoint, bodyBottomCenterPoint, fishAngle) {
        this.canvas.globalAlpha = 160 / 255 //加深鱼身
        // 身体的四个点
        let topLeftPoint = MathUtils.calculatePoint(headPoint, this.HEAD_RADIUS, fishAngle + 90);
        let topRightPoint = MathUtils.calculatePoint(headPoint, this.HEAD_RADIUS, fishAngle - 90);
        let bottomLeftPoint = MathUtils.calculatePoint(bodyBottomCenterPoint, this.BIG_CIRCLE_RADIUS,
            fishAngle + 90);
        let bottomRightPoint = MathUtils.calculatePoint(bodyBottomCenterPoint, this.BIG_CIRCLE_RADIUS,
            fishAngle - 90);

        // 二阶贝塞尔曲线的控制点
        let controlLeft = MathUtils.calculatePoint(headPoint, this.BODY_LENGTH * 0.56, fishAngle + 130);
        let controlRight = MathUtils.calculatePoint(headPoint, this.BODY_LENGTH * 0.56, fishAngle - 130);

        // 画鱼身
        drawPath(canvas, ['M', topLeftPoint, "Q", controlLeft, bottomLeftPoint, "L", bottomRightPoint, "Q", controlRight, topRightPoint])
        this.canvas.globalAlpha = 100 / 255
    },
    /**
     * 画节肢
     *
     * @param bottomCenterPoint     梯形底部的中心点坐标（长边）
     * @param bigRadius             大圆的半径
     * @param smallRadius           小圆的半径
     * @param findSmallCircleLength 寻找梯形小圆的线长
     * @param isBigCircle          是否有大圆
     */
    drawSegment(canvas, bottomCenterPoint, bigRadius,
                smallRadius, findSmallCircleLength, fishAngle,
                isBigCircle) {
        canvas.fillStyle = this.defaultFillStyle
        // 节肢摆动的角度
        let segmentAngle = 0;
        // 节肢1 用 cos
        if (isBigCircle) {
            segmentAngle = (fishAngle + Math.cos(MathUtils.toRadians(this.swimBuff*this.currentValue * 1.2)) * 15);
        } else {
            segmentAngle = (fishAngle + Math.sin(MathUtils.toRadians(this.swimBuff*this.currentValue * 1.2)) * 35);
        }
        // 梯形上底的中心点（短边）
        let upperCenterPoint = MathUtils.calculatePoint(bottomCenterPoint, findSmallCircleLength, segmentAngle - 180);
        // 梯形的四个顶点
        let bottomLeftPoint = MathUtils.calculatePoint(bottomCenterPoint, bigRadius, segmentAngle + 90);
        let bottomRightPoint = MathUtils.calculatePoint(bottomCenterPoint, bigRadius, segmentAngle - 90);
        let upperLeftPoint = MathUtils.calculatePoint(upperCenterPoint, smallRadius, segmentAngle + 90);
        let upperRightPoint = MathUtils.calculatePoint(upperCenterPoint, smallRadius, segmentAngle - 90);

        if (isBigCircle) {
            // 绘制大圆
            drawCircle(canvas, bottomCenterPoint, bigRadius);
        }
        // 绘制小圆
        drawCircle(canvas, upperCenterPoint, smallRadius);

        // 绘制梯形
        drawPath(canvas, ['M', bottomLeftPoint, 'L', upperLeftPoint, 'L', upperRightPoint, 'L', bottomRightPoint])
        return upperCenterPoint;
    },
    drawEye(canvas, fishAngle) {
        canvas.fillStyle = 'rgba(0, 0,0,1)'
        //左眼
        canvas.beginPath();
        let leftEye = MathUtils.calculatePoint(this.headPoint, this.HEAD_RADIUS * 0.7, fishAngle + 45)
        canvas.ellipse(leftEye.x, leftEye.y,
            this.HEAD_RADIUS * 0.12, this.HEAD_RADIUS * 0.07, -Math.PI * 0.3,
            Math.PI * 0, Math.PI * 2, 1
        );
        canvas.fill();
        //右眼
        canvas.beginPath()
        let rightEye = MathUtils.calculatePoint(this.headPoint, this.HEAD_RADIUS * 0.7, fishAngle - 45)
        canvas.ellipse(rightEye.x, rightEye.y,
            this.HEAD_RADIUS * 0.12, this.HEAD_RADIUS * 0.07, Math.PI * 0.3,
            Math.PI * 0, Math.PI * 2, 1
        );
        canvas.fill();
    },
    clearCanvas() {
        this.canvas.clearRect(0, 0, this.width(), this.height());
    },
    width() {
        return this.HEAD_RADIUS * 8.38
    },
    height() {
        return this.HEAD_RADIUS * 8.38
    },
    drawHead(canvas, fishAngle) {
        canvas.fillStyle = 'rgba(244, 92,71,0.5)'
        drawCircle(canvas, this.headPoint, this.HEAD_RADIUS)
        this.drawEye(canvas, fishAngle)
    },
    drawFins(canvas, startPoint, length, fishAngle, isRightFins) {
        canvas.fillStyle = 'rgba(244, 92,71,0.5)'
        let controlAngle = 115+Math.sin(MathUtils.toRadians(this.currentValue*this.swimBuff))*10;
        // 结束点
        let endPoint = MathUtils.calculatePoint(startPoint, length, fishAngle - 180);
        // 控制点
        let controlPoint = MathUtils.calculatePoint(startPoint, 1.8 * length, isRightFins ?
            fishAngle - controlAngle:
            fishAngle + controlAngle
        )
        drawPath(canvas, ['M', startPoint, 'Q', controlPoint, endPoint])
    },
    drawTriangle(canvas, startPoint, findCenterLength, findEdgeLength, fishAngle) {
        canvas.fillStyle = 'rgba(244, 92,71,0.5)'
        // 三角形鱼尾的摆动角度需要跟着节肢2走
        let triangleAngle = (fishAngle + Math.sin(MathUtils.toRadians(this.swimBuff*this.currentValue * 1.2)) * 35);

        // 底部中心点的坐标
        let centerPoint = MathUtils.calculatePoint(startPoint, findCenterLength, triangleAngle - 180);
        // 三角形底部两个点
        let leftPoint = MathUtils.calculatePoint(centerPoint, findEdgeLength, triangleAngle + 90);
        let rightPoint = MathUtils.calculatePoint(centerPoint, findEdgeLength, triangleAngle - 90);
        // 绘制三角形
        drawPath(canvas, ['M', startPoint, 'L', leftPoint, 'L', rightPoint])
        //console.log(`makeTriangle#startPoint:${JSON.stringify(startPoint)},leftPoint:${JSON.stringify(leftPoint)},rightPoint:${JSON.stringify(rightPoint)}`)
    },

    setHeadRadius(r) {
        this.HEAD_RADIUS = r
        this.middlePoint = {
            x: 4.19 * this.HEAD_RADIUS,
            y: 4.19 * this.HEAD_RADIUS,
        }
        // 鱼身的长度
        this.BODY_LENGTH = 3.2 * this.HEAD_RADIUS;

        this.FIND_FINS_LENGTH = 0.9 * this.HEAD_RADIUS;

        this.FINS_LENGTH = 1.3 * this.HEAD_RADIUS;

        // -------------鱼尾---------------
        // 尾部大圆的半径(圆心就是身体底部的中点)
        this.BIG_CIRCLE_RADIUS = this.HEAD_RADIUS * 0.7;
        // 尾部中圆的半径
        this.MIDDLE_CIRCLE_RADIUS = this.BIG_CIRCLE_RADIUS * 0.6;
        // 尾部小圆的半径
        this.SMALL_CIRCLE_RADIUS = this.MIDDLE_CIRCLE_RADIUS * 0.4;
        // --寻找尾部中圆圆心的线长
        this.FIND_MIDDLE_CIRCLE_LENGTH = this.BIG_CIRCLE_RADIUS + this.MIDDLE_CIRCLE_RADIUS;
        // --寻找尾部小圆圆心的线长
        this.FIND_SMALL_CIRCLE_LENGTH = this.MIDDLE_CIRCLE_RADIUS * (0.4 + 2.7);
        // --寻找大三角形底边中心点的线长
        this.FIND_TRIANGLE_LENGTH = this.MIDDLE_CIRCLE_RADIUS * 2.7;
    },
}
//绘制路径
function drawPath(canvas, points) {
    canvas.beginPath()
    for (let i = 0; i < points.length; ) {
        if (typeof (points[i]) == 'string') {
            if (points[i].toUpperCase() == 'M') {
                i += 1
                canvas.moveTo(points[i].x, points[i].y);
            } else if (points[i].toUpperCase() == 'L') {
                i += 1
                canvas.lineTo(points[i].x, points[i].y);
            } else if (points[i].toUpperCase() == 'Q') {
                i += 2
                canvas.quadraticCurveTo(points[i-1].x, points[i-1].y, points[i].x, points[i].y);
            } else if (points[i].toUpperCase() == 'C') {
                i += 3
                canvas.bezierCurveTo(points[i-2].x, points[i-2].y, points[i-1].x, points[i-1].y, points[i].x, points[i].y);
            } else if (points[i].toUpperCase() == 'Z') {
            }
            i++
        } else {
            throw `points[${i}]--->is not command string,value is ${points[i]}`;
        }
    }
    canvas.closePath()
    canvas.fill();
}
//绘制圆
function drawCircle(canvas, center, r, radians=6.28) {
    canvas.beginPath()
    canvas.arc(center.x, center.y, r, 0, radians)
    canvas.closePath()
    canvas.fill() //填充
}

