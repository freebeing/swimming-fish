import animator from '@ohos.animator';
import MathUtils from '../utils/MathUtils.js';

export default {
    props: {
        radius: 0,
        x: 0,
        y: 0,
        time: 0
    },
    data() {
        return {
            radius: this.radius,
            canvas: null,
            x: this.x,
            y: this.y,
            alpha: 1,
            time: this.time
        }
    },

    onAttached() {
        setTimeout(() => {
            this.canvas = this.$refs.ripple.getContext('2d', {
                antialias: true
            })
            this.onDraw()
            this.startAnimator()
        }, 200)
    },
    startAnimator() {
        let options = {
            duration: 1000,
            direction: "normal",
            begin: 0,
            end: 1,
            fill:'forwards'
        }
        let anim = animator.createAnimator(options);
        anim.onframe = value => {
            //console.log("startAnimation onframe====>" + value);
            this.radius = 150 * value
            this.alpha = (1 - value)
            this.onDraw()
        }
        anim.onfinish = () => {
            this.$emit('rippleFinish', {
                x: this.x,
                y: this.y,
                time: this.time
            });
        }
        anim.play()
    },


    onDraw() {
        this.canvas.clearRect(0, 0, 10000, 10000);
        //this.drawCircle(this.canvas, this.radius, this.radius, this.radius)
        this.drawRipple(this.canvas, this.x, this.y, this.radius)
    },
    drawRipple(canvas, x, y, r) {
        //console.log('ripple_x:' + x + ",y:" + y + ",r:" + r)
        canvas.beginPath()
        canvas.strokeStyle = `rgba(0,0,0,${this.alpha})`
        canvas.arc(x, y, r, 0, MathUtils.toRadians(360))
        canvas.stroke()
        canvas.closePath()
    },
}
