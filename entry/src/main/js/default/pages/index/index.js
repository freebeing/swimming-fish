import Log from '../utils/Log.js';
import MathUtils from '../utils/MathUtils.js';
import animator from '@ohos.animator';

const TAG = "FishRelativeLayout"

export default {
    data() {
        return {
            title: "",
            animator: null,
        }
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    touchCancel(event) {
        //console.log(`touchCancel:${event.locaX},${event.locaY}`)
    },
    touchEnd(event) {
        let fish = this.$child('fish');
        let fishImageViewMiddle = fish.getMiddlePoint();
        let toPoint = this.getPoints(event.changedTouches[0]);
        console.log(`touchEnd:${toPoint.x},${toPoint.y}`)
        let touchX = toPoint.x - fishImageViewMiddle.x
        let touchY = toPoint.y - fishImageViewMiddle.y
        //touchX = 300;
        //touchY = 400;
        toPoint.x = touchX
        toPoint.y = touchY
        let fromPoint = fish.getPosition()
        let angle2 = Math.atan2((toPoint.y - fromPoint.y),
            toPoint.x - fromPoint.x
        )
        let headPoint = fish.getHeadPoint(MathUtils.toDegrees(-angle2))
        toPoint.x -= (headPoint.x-fishImageViewMiddle.x)
        toPoint.y -= (headPoint.y-fishImageViewMiddle.y)
        var options = {
            duration: MathUtils.dist(fromPoint, toPoint) * 5,
            easing: 'ease',
            iterations: 1,
            fill: "forwards",
            begin: 0,
            end: 1
        };
        if (this.animator != null) {
            this.animator.cancel()
        }
        let fishMiddle = fish.getCenterPosition();
        let touch = toPoint;
        let fishHead = fish.getHeadPosition()
        let angle = MathUtils.includedAngle(fishMiddle, fishHead, touch);
        let delta = MathUtils.includedAngle(fishMiddle, {
            x: fishMiddle.x + 1, y: fishMiddle.y
        }, fishHead);
        // 控制点2
        let controlPoint1 = {
            x: fishHead.x - fishImageViewMiddle.x,
            y: fishHead.y - fishImageViewMiddle.y
        };
        let controlPoint = MathUtils.calculatePoint(fishMiddle, fish.getHeadRadius() * 1.6, angle / 2 + delta);
        controlPoint.x -= fishImageViewMiddle.x
        controlPoint.y -= fishImageViewMiddle.y

        this.animator = animator.createAnimator(options)
        this.animator.onframe = (value) => {
            let pos = MathUtils.threeBezier(value,
                fromPoint,
                controlPoint1,
                controlPoint,
                toPoint)
            //console.log('pos---->' + JSON.stringify(pos))
            fish.move(pos.x, pos.y)
            let tang = MathUtils.threeBezierTangent(value, fromPoint, controlPoint1, controlPoint, toPoint)
            fish.setFishMainAngle(MathUtils.toDegrees(Math.atan2(-tang.y, tang.x)))
            //fish.moveByCenter(fromPoint.x + (toPoint.x - fromPoint.x) * value,fromPoint.y + (toPoint.y - fromPoint.y) * value)
        }
        this.animator.onfinish = () => {
            this.animator = null
        }
        this.animator.oncancel = () => {

            this.animator = null
        }
        this.animator.play()
        console.log(`touchEnd:+${JSON.stringify(fromPoint)},${JSON.stringify(toPoint)},${JSON.stringify(fromPoint + toPoint)}`)
    },
    // 记录前一个点
    getPoints(res) {
        return {
            x: res.localX,
            y: res.localY,
        };
    },

    touchMove(event) {
    },
    touchStart(event) {
    },
}
